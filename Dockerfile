FROM alpine:3.11

# Environments
ENV TIMEZONE Asia/Taipei

# Install packages
RUN	apk update && \
	apk upgrade && \
	apk add --update tzdata && \
	cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime && \
	echo "${TIMEZONE}" > /etc/timezone && \
	apk add \
        openssh-client \
        rsync \
        ansible


# Cleaning up
RUN \
    apk del tzdata && \
    rm -rf /var/cache/apk/*
